
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
window.Vue.use(VueRouter);

import axios from 'axios';
import VueAxios from 'vue-axios';
window.Vue.use(VueAxios, axios);

import Vuex from 'vuex';
window.Vue.use(Vuex);

import storePlugin from './components/store/storePlugin';
window.Vue.use(storePlugin)



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import exampleComponent from './components/ExampleComponent.vue';
import LoginComponent from './components/LoginComponent.vue';
import RegistrationComponent from './components/RegistrationComponent.vue'
import DashBoardComponent from './components/DashboardComponent.vue'

const routes = [
    {
        path:'/',
        component:exampleComponent,
    },
    {
        path:'/login',
        component:LoginComponent,
    },
    {
        path:'/register',
        component:RegistrationComponent,
    },
    {
        path:'/dashBoard',
        component:DashBoardComponent,
        meta:{
            requiresAuth:true
        }
    },
];

const router = new VueRouter({
    routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      let token = JSON.parse(localStorage.getItem('token'));
      if(token == null) {
        next({
          path: '/login',
        })
      } else {
        next()
      }
    } else {
      next()
    }
});

const app = new Vue({
    router,
    mode:'history',
}).$mount('#app')
