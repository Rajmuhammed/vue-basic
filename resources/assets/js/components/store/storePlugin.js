import store from './store';
window.Vue = require('vue');

export default {
    store,
    install (Vue, options) {
        Vue.prototype.$myStore = store
    }
}