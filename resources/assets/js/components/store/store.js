
window.Vue = require('vue');

import Vuex from 'vuex';
window.Vue.use(Vuex);

const store = new Vuex.Store({
    state:{
        isLoggedIn:false,
    },
    mutations:{
        changeLoggedInStatus(state){
            let token = JSON.parse(localStorage.getItem('token'));
            if(token != null) {
                state.isLoggedIn = true;
            } else {
                state.isLoggedIn = false;
            }
        },
    },
    getters:{
        checkIsLoggedIn(state){
            return state.isLoggedIn;
        },
    }
});

export default store;

//Calling Mutation Store.commit('mutation name)
//Calling getters Store.getters.function_name