<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use Response;
use Carbon\Carbon;


class PassportController extends Controller
{
    public function login(Request $request)
    {

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (! Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
            

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
    
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'firstname'=>'required|string',
            'lastname'=>'required|string',
            'password'=>'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return Response::json(['errors' => $validator->errors()], 422);
        } else {

            $user = new User([
                'email' => $request->email,
                'name' => $request->firstname .' '.$request->lastname,
                'password' => bcrypt($request->password),
                'first_name' => $request->firstname,
                'last_name' => $request->lastname
            ]);

            $user->save();
            return response()->json([
                'message' => 'Successfully created user!'
            ], 201);
        }

    }

    public function logout(Request $request)
    {
       
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function user(Request $request)
    {
        return response()->json($request->user());
    }


}
